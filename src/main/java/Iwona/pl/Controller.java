package Iwona.pl;

import java.util.Scanner;

public class Controller {

    private Scanner input;
    private int number;
    private String[] words;

    private void read() {
        input = new Scanner(System.in);
        System.out.println("Podaj liczbę wyrazów: ");
        number = input.nextInt();
        input.nextLine();
    }

    private void wordsList() {
        read();
        words = new String[number];
        for (int i = 0; i < number; i++) {
            System.out.println("Podaj wyraz: ");
            words[i] = input.nextLine();
        }
    }

    public StringBuilder build() {
        wordsList();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < words.length; i++) {
            stringBuilder.append(words[i].charAt(words[i].length() - 1));
        }
        input.close();
        return stringBuilder;
    }
}
